import pytest
from home import State, weighted_choice


def test_state_machine():
    counter = 0
    for state in State():
        print(state)
        if counter > 100:
            break
        counter += 1


def test_weighted_choice():
    for arg in [
        [(10, 1), (10, 2)],
        [(10, 1)],
        [(10, 1), (10, 2), (80, 3)],
        [(100, 1), (10, 2), (80, 3)],
        [(0, 1), (10, 2), (80, 3)],
        [(10, 1), (0, 2), (0, 3)],
    ]:
        assert weighted_choice(arg) in (1, 2, 3)

    with pytest.raises(ValueError):
        weighted_choice([])

    with pytest.raises(ValueError):
        weighted_choice([(0, 1)])

    with pytest.raises(ValueError):
        weighted_choice([(0, 1), (0, 2), (0, 3)])

    assert weighted_choice([(100, 1), (0, 2)]) == 1
    assert weighted_choice([(0, 2), (1, 1)]) == 1
    assert weighted_choice([(1, 1)]) == 1
