#!/usr/bin/env python3

import struct
import sys
from pathlib import Path

import av

p = Path(sys.argv[1])
container = av.open(str(p))

with open(p.with_suffix(".mvi").name, "wb") as outfile:
    for i, frame in enumerate(container.decode(video=0)):
        if i % 100 == 0:
            print(f"Reading frame {i}...")

        img = frame.to_image()
        img.thumbnail((1, 1))
        color = img.getpixel((0, 0))
        outfile.write(struct.pack("BBB", *color))
