#!/usr/bin/env python3


import sys
from time import sleep

import pygame

sys.path.append(".")

from utils import tv_colors  # noqa


def main():
    pygame.init()
    screen = pygame.display.set_mode((100, 100))

    for red, green, blue in tv_colors(sys.argv[1]):
        pygame.draw.rect(screen, pygame.Color(red, green, blue), [0, 0, 100, 100])
        pygame.display.flip()
        sleep(0.040)


if __name__ == "__main__":
    main()
