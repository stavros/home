import time

from utils import Timer, correct_gamma, hsv2rgb, randrange, tv_colors, value_lerper, weighted_choice

try:
    from machine import Pin, RTC
    from neopixel import NeoPixel
except ImportError:
    pass


MILLISECS_PER_FRAME = 10

# How many LEDs there are on the strip.
LEDS = 6

# How many seconds to wait between person updates.
SPEED = 30

# How long to wait before switching off the previous switch.
SWITCH_OFF = 1


class State:
    LIVING_ROOM = 0
    WATCHING_TV = 5
    GOING_UP = 10
    WENT_UP = 11
    GOING_DOWN = 15
    WENT_DOWN = 16
    READING_ROOM = 20
    READING = 25
    ATTIC = 30
    GARAGE = 40

    STATE_TRANSITIONS = {
        LIVING_ROOM: [(5, LIVING_ROOM), (60, WATCHING_TV), (35, GOING_DOWN)],
        WATCHING_TV: [(95, WATCHING_TV), (5, LIVING_ROOM)],
        GOING_UP: [(95, WENT_UP), (5, READING_ROOM)],
        WENT_UP: [(70, LIVING_ROOM), (30, ATTIC)],
        GOING_DOWN: [(95, WENT_DOWN), (5, LIVING_ROOM)],
        WENT_DOWN: [(90, READING_ROOM), (10, GARAGE)],
        READING_ROOM: [(10, GOING_UP), (90, READING)],
        READING: [(90, READING), (10, READING_ROOM)],
        ATTIC: [(40, ATTIC), (60, WENT_UP)],
        GARAGE: [(40, GARAGE), (60, WENT_DOWN)],
    }

    def __init__(self):
        self._current_state = self.READING_ROOM

    def __iter__(self):
        return self

    def __next__(self):
        next_state = weighted_choice(self.STATE_TRANSITIONS[self._current_state])
        self._current_state = next_state
        return self._current_state


class Strip:
    def __init__(self, pin=0, num_pixels=LEDS):
        self._np = NeoPixel(Pin(pin, Pin.OUT), num_pixels)
        self.clear()

    def clear(self):
        self._np.fill([0, 0, 0])
        self._np.write()

    def set_pixel(self, pixel, value, gamma=2.8):
        """Set a pixel to the given RGB value (a tuple of (red, green, blue))."""
        self._np[pixel] = correct_gamma(value, gamma)
        self._np.write()

    def set_pixel_random(self, pixel):
        """Set a pixel to a random color."""
        self.set_pixel(pixel, (randrange(256), randrange(256), randrange(256)))

    def set_pixel_hsv(self, pixel, value):
        """Set a pixel to the given HSV value (a tuple of (hue, saturation, value))."""
        self.set_pixel(pixel, hsv2rgb(*value))


class Home:
    def __init__(self, strip):
        self._strip = strip
        strip.clear()

        self._t_person = Timer()
        self._t_lights = Timer()
        self._t_clear = Timer(one_shot=True)

        self._fireplace = value_lerper(
            hue=35, saturation=1, ms_per_frame=MILLISECS_PER_FRAME, ms_per_change=150, rand_factor=15
        )
        self._tv = tv_colors("/movies/ninenine.mvi")

        # The ghost, use this as `set_pixel(4, hsv2rgb(*next(l)), gamma=1)`.
        self._ghost = value_lerper(
            hue=35, saturation=0, ms_per_frame=MILLISECS_PER_FRAME, ms_per_change=2000, rand_factor=3, offset=0.01
        )

    def leds_from_state(self, state):
        DOWNSTAIRS = 0
        READING_ROOM = 1
        LIVING_ROOM = 2
        UPSTAIRS = 3
        ATTIC = 4
        GARAGE = 5

        WHITE = (40, 0.35, 1)

        strip = self._strip

        if state == State.LIVING_ROOM:
            strip.set_pixel_hsv(LIVING_ROOM, WHITE)
        elif state == State.WATCHING_TV:
            strip.set_pixel(LIVING_ROOM, next(self._tv))
        elif state == State.GOING_UP:
            strip.set_pixel_hsv(DOWNSTAIRS, WHITE)
            strip.set_pixel_hsv(UPSTAIRS, WHITE)
        elif state == State.WENT_UP:
            strip.set_pixel_hsv(DOWNSTAIRS, WHITE)
            strip.set_pixel_hsv(UPSTAIRS, WHITE)
        elif state == State.GOING_DOWN:
            strip.set_pixel_hsv(DOWNSTAIRS, WHITE)
            strip.set_pixel_hsv(UPSTAIRS, WHITE)
        elif state == State.WENT_DOWN:
            strip.set_pixel_hsv(DOWNSTAIRS, WHITE)
            strip.set_pixel_hsv(UPSTAIRS, WHITE)
        elif state == State.READING_ROOM:
            strip.set_pixel_hsv(READING_ROOM, WHITE)
        elif state == State.READING:
            strip.set_pixel_hsv(READING_ROOM, next(self._fireplace))
        elif state == State.ATTIC:
            strip.set_pixel_hsv(ATTIC, WHITE)
        elif state == State.GARAGE:
            strip.set_pixel_hsv(GARAGE, WHITE)

    def demo(self):
        """Turn all the LEDs random colors."""
        for led in range(LEDS):
            self._strip.set_pixel_random(led)

    def rdemo(self):
        """Turn a random LED a random color."""
        self._strip.set_pixel_random(randrange(LEDS))

    def live(self, duration_min=None):
        """
        Live in this home.

        duration - How long, in minutes, to run for.
        """
        last_state = None
        state_generator = State()
        state = next(state_generator)
        t = Timer()

        while duration_min is None or not t.has_passed(duration_min * 60 * 1000):
            if not self._t_lights.has_passed(MILLISECS_PER_FRAME):
                # We're limiting our fps, so wait for a frame.
                time.sleep_ms(1)
                continue

            if self._t_person.has_passed(SPEED * 1000):
                state = next(state_generator)

                print(state)

                if state != last_state:
                    # If the state changed, clear the strip later, to give the impression that
                    # the switches are lagging.
                    self._t_clear.reset()

                last_state = state

            if self._t_clear.has_passed(SWITCH_OFF * 1000):
                self._strip.clear()

            self.leds_from_state(state)

        self._strip.clear()


def run():
    rtc = RTC()
    target_minute = randrange(60)

    while True:
        hour, minute = rtc.datetime()[4:6]

        if (hour, minute) < (16, target_minute):
            time.sleep(1)
            continue

        # We want this to last between five and eight hours.
        Home(Strip()).live(5 * 60 + randrange(3 * 60))
        print("Finished simulation run for the day.")
        target_minute = randrange(60)


def run_continuously():
    Home(Strip()).live()
