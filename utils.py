import math

try:
    import uos
    import ustruct
    from time import ticks_ms, ticks_diff, sleep, localtime
except ImportError:
    import struct as ustruct
    import os as uos
    from time import sleep, localtime, time

    def ticks_ms():
        return int(time() * 1000)

    def ticks_diff(x, y):
        return x - y


class Timer:
    def __init__(self, one_shot=False, initial_state=False):
        self._one_shot = one_shot
        self._initial_state = initial_state
        self.reset()

    def reset(self):
        self._ticks = ticks_ms()
        self._fired = False

    def has_passed(self, ms):
        if self._initial_state:
            self._initial_state = False
            return True

        if self._one_shot and self._fired:
            return False

        ticks = ticks_ms()
        if ticks_diff(ticks, self._ticks) > ms:
            self.reset()
            self._fired = True
            return True
        return False


def randrange(top):
    number = ustruct.unpack("I", uos.urandom(4))[0]
    return int((top * number) / 256 ** 4)


def random():
    return randrange(1000) / 1000


def hsv2rgb(h, s, v):
    h = float(h)
    s = float(s)
    v = float(v)
    h60 = h / 60.0
    h60f = math.floor(h60)
    hi = int(h60f) % 6
    f = h60 - h60f
    p = v * (1 - s)
    q = v * (1 - f * s)
    t = v * (1 - (1 - f) * s)
    r, g, b = 0, 0, 0
    if hi == 0:
        r, g, b = v, t, p
    elif hi == 1:
        r, g, b = q, v, p
    elif hi == 2:
        r, g, b = p, v, t
    elif hi == 3:
        r, g, b = p, q, v
    elif hi == 4:
        r, g, b = t, p, v
    elif hi == 5:
        r, g, b = v, p, q
    r, g, b = int(r * 255), int(g * 255), int(b * 255)
    return r, g, b


def tv_colors(filename):
    t = Timer(initial_state=True)
    value = (0, 0, 0)
    with open(filename, "rb") as f:
        while True:
            # Play at 25 FPS.
            if not t.has_passed(1000 / 25):
                yield value

            data = f.read(3)
            if not data:
                f.seek(0)
                data = f.read(3)
            value = ustruct.unpack("BBB", data)
            yield value


def value_lerper(hue, saturation, ms_per_frame, ms_per_change=100, offset=0.5, rand_factor=10):
    """
    Generate and interpolate values.

    This function generates a random value for a color and then slowly lerps from the old to the new value.
    """
    t = Timer(initial_state=True)
    counter = 0
    changes = int(ms_per_change / ms_per_frame)
    next_mod = randrange(rand_factor) / 100

    while True:
        counter = (counter + 1) % changes

        if t.has_passed(ms_per_change):
            mod = next_mod
            next_mod = randrange(rand_factor) / 100
            counter = 0

        percentage_through = counter / changes
        interpolated = mod * (1 - percentage_through) + next_mod * percentage_through
        yield (hue, saturation, offset + interpolated)


def fireplace(strip, led, hue=35, iterations=100, offset=0.5, duration=0.1, rrange=10):
    """A test function that emulates a fireplace."""
    for x in range(iterations):
        strip.set_pixel(led, hsv2rgb(hue, 1, offset + (randrange(rrange) / 100)))
        sleep(duration)


def set_time():
    import untplib
    from machine import RTC

    resp = untplib.NTPClient().request("gr.pool.ntp.org", version=3, port=123)
    year, month, day, hours, minutes, seconds, weekday, subsecs = localtime(resp.recv_time)
    RTC().datetime([year, month, day, weekday, hours, minutes, seconds, subsecs])
    return resp.offset


def correct_gamma(color, gamma=2.8):
    return tuple(int(((intensity / 255) ** gamma) * 255) for intensity in color)


def weighted_choice(choices):
    """
    Choose an option from an iterable of weighted choices.

    The iterable must be of the form:
    [(weight: int, choice: Any)]
    """
    if not choices:
        raise ValueError("no choices given")

    weight_sum = sum(weight for weight, choice in choices)
    if weight_sum <= 0:
        raise ValueError("invalid choice weights")

    random_number = randrange(weight_sum)
    rolling_sum = 0
    for weight, choice in choices:
        rolling_sum += weight
        if rolling_sum > random_number:
            return choice

    raise ValueError("no choice chosen")
