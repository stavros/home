Home
====

Home is a project that aims to simulate a tiny person living in a decorative Christmas house.


Details
=======

`home.py` is the main file, you need to call `Home(Strip()).live()` in the `boot.py`, and the simulation will start on boot.

There's a `movies` directory with movie colors, but it's not used.
